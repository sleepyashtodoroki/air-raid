package ru.androiddevschool.airraid.Model;

import com.badlogic.gdx.Input;

import java.util.HashSet;

import ru.androiddevschool.airraid.View.ImageActor;

/**
 * Created by ga_nesterchuk on 11.02.2017.
 */
public class Player extends ImageActor {
    private HashSet<Integer> pressedButtons;
    private float v;
    private int lives;

    public Player(String filename, float x, float y, float width, float height, HashSet<Integer> pressedButtons) {
        super(filename, x, y, width, height);
        this.pressedButtons = pressedButtons;
        v = 100;
        lives = 100;
    }

    public void hit(){
        lives--;
        System.out.println(lives);
    }

    public void act(float delta) {
        super.act(delta);
        if (pressedButtons.contains(Input.Keys.UP))
            moveBy(
                    v * delta * (float) Math.cos(Math.toRadians(getRotation())),
                    v * delta * (float) Math.sin(Math.toRadians(getRotation()))
            );
        if (pressedButtons.contains(Input.Keys.DOWN))
            moveBy(
                    -v * delta * (float) Math.cos(Math.toRadians(getRotation())),
                    -v * delta * (float) Math.sin(Math.toRadians(getRotation()))
            );
        if (pressedButtons.contains(Input.Keys.LEFT)) rotateBy(v * delta);
        if (pressedButtons.contains(Input.Keys.RIGHT)) rotateBy(-v * delta);
        if (pressedButtons.contains(Input.Keys.SPACE)) shoot();
        getStage().getViewport().getCamera().position.x = getX();
        getStage().getViewport().getCamera().position.y = getY();
    }
    public void shoot(){

    }
}
