package ru.androiddevschool.airraid.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import ru.androiddevschool.airraid.View.ImageActor;

/**
 * Created by ga_nesterchuk on 11.02.2017.
 */
public class Mob extends ImageActor {
    Player player;
    float v;
    public Mob(String filename, float x, float y, Player player) {
        super(filename, x, y);
        this.player = player;
        this.v = 80;
    }

    public void act(float delta){
        setRotation(
                (float) Math.toDegrees(
                        Math.atan2(player.getY() - getY(), player.getX() - getX())
                )
        );
        moveBy(
                v * delta * (float) Math.cos(Math.toRadians(getRotation())),
                v * delta * (float) Math.sin(Math.toRadians(getRotation()))
        );
        if (Vector2.dst2(getX(),getY(),player.getX(),player.getY())<v*delta){
            player.hit();
            remove();
        }

    }
}
