package ru.androiddevschool.airraid.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashSet;

import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.View.Background;

/**
 * Created by ga_nesterchuk on 11.02.2017.
 */
public class World extends Stage {
    private Player player;
    private HashSet<Integer> pressedButtons;
    private Vector2 stageCoords;
    public World(ScreenViewport screenViewport, SpriteBatch batch) {
        super(screenViewport, batch);
        pressedButtons = new HashSet<Integer>();
        addActor(new Background(Assets.get().images.get("space").getRegion().getTexture()));
        player = new Player("player.png", 200, 200, 80, 80, pressedButtons);
        addActor(player);
        stageCoords = new Vector2();
    }
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        if (super.touchDown(screenX, screenY, pointer, button)) return true;
        stageCoords.set(screenX, screenY);
        screenToStageCoordinates(stageCoords);
        addActor(new Mob("mob.png", stageCoords.x, stageCoords.y, player));
        return true;
    }
    public boolean keyDown (int keycode) {
        return pressedButtons.add(keycode);
    }
    public boolean keyUp (int keycode) {
        return pressedButtons.remove(keycode);
    }
}
