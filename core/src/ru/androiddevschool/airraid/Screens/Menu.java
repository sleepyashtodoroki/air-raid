package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.ScreenTraveler;

/**
 * Created by ga_nesterchuk on 04.02.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new TextButton("Play", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler("Play"));
        table.add(button).pad(10).row();
        button = new TextButton("Help", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler("Help"));
        table.add(button).pad(10).row();
        button = new TextButton("Rating", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler("Rating"));
        table.add(button).pad(10).row();
        button = new TextButton("Settings", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler("Settings"));
        table.add(button).pad(10).row();

        stage.addActor(table);
    }
}