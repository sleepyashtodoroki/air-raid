package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import ru.androiddevschool.airraid.Model.World;
import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.ScreenTraveler;

/**
 * Created by ga_nesterchuk on 04.02.2017.
 */
public class Play extends StdScreen{

    public Play(SpriteBatch batch) {
        super(batch);
        this.stage = new World(new ScreenViewport(camera), batch);

        Table table = new Table();
        table.setFillParent(true);
        //table.setDebug(true);
        ImageButton button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("small green exit"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).expandX().top().right().padRight(20).padTop(20).row();
        table.add().expand(true,true).row();
        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("small green shoot"));
        table.add(button).expandX().right().bottom().padRight(20).padBottom(20).row();

        ui.addActor(table);

    }

}