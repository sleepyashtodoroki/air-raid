package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by ga_nesterchuk on 25.02.2017.
 */
class StdScreen implements Screen { //класс меню, который является экраном
    protected OrthographicCamera camera; //камера, которая будет смотреть на сцену
    protected Stage stage; //сама сцена
    protected OrthographicCamera uicamera; //камера, которая будет смотреть на геймплей
    protected Stage ui; //интерфейс - наши кнопки
    protected InputMultiplexer multiplexer;
    public StdScreen(SpriteBatch batch) {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //создаем камеру и задаем размеры
        camera.setToOrtho(false); //направляем ось Oy вниз
        stage = new Stage(new ScreenViewport(camera), batch); //создаем сцену с камерой и отрисовщиком
        uicamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //создаем камеру и задаем размеры
        uicamera.setToOrtho(false); //направляем ось Oy вниз
        ui = new Stage(new ScreenViewport(uicamera), batch); //создаем сцену с камерой и отрисовщиком
    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer); //все нажатия на libGDX Будет обрабатывать сцена этого экрана
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //закрашиваем экран белым цветом
        ui.act(delta);
        stage.act(delta); //сдвинуть всех актеров на сцене
        stage.draw(); //отрисовать сцену
        ui.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null); //сцена прекращает обработку нажатия
    }

    @Override
    public void dispose() {
        stage.dispose(); // при умирании экрана убить сцену
    }
}
