package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.ScreenTraveler;

/**
 * Created by ga_nesterchuk on 04.02.2017.
 */
public class Settings extends StdScreen{

    public Settings(SpriteBatch batch) {
        super(batch);
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("blue home"));
        button.addListener(new ScreenTraveler("Menu"));
        button.left().top();
        table.left().top().add(button).row();

        stage.addActor(table);
    }
}