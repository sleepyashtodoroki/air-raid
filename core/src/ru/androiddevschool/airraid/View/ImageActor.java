package ru.androiddevschool.airraid.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

/**
 * Created by ga_nesterchuk on 04.02.2017.
 */
public class ImageActor extends Actor {
    private TextureRegion img;
//начало разных конструкторов
    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x, y);
        setSize(width, height);
        setOrigin(width / 2, height / 2);
    }
    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }
    public ImageActor(TextureRegion img) {
        this(img, 0, 0, img.getRegionWidth(), img.getRegionHeight());
    }
    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }
    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y);
    }
    public ImageActor(Texture img) {
        this(new TextureRegion(img));
    }
    public ImageActor(String filename, float x, float y, float width, float height) {
        this(new Texture(filename), x, y, width, height);
    }
    public ImageActor(String filename, float x, float y) {
        this(new Texture(filename), x, y);
    }
    public ImageActor(String filename) {
        this(new Texture(filename));
    }
//конец разных конструкторов

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img,
                getX()-getOriginX(), getY()-getOriginY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                getRotation()-90);
    }
}
