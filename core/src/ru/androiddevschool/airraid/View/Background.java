package ru.androiddevschool.airraid.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by ga_nesterchuk on 07.03.2017.
 */
public class Background extends Actor {
    private Texture src;

    public Background(Texture src) {
        super();
        this.src = src;
        src.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        setPosition(0, 0);
        setSize(1000, 1000);
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(src, getX(), getY(), 0, 0, (int)getWidth(), (int)getHeight());
    }
}
