package ru.androiddevschool.airraid.Utils;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.airraid.AirRaid;

/**
 * Created by ga_nesterchuk on 04.03.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y) {//куда кнопка будет переводить
        AirRaid.getInstance().setScreen(name);
    }
}
