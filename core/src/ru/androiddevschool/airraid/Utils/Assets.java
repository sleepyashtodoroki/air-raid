package ru.androiddevschool.airraid.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.io.File;
import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 04.03.2017.
 */

public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initStyles();
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("long green text",
                new TextButton.TextButtonStyle(
                        images.get("green_button04"),
                        images.get("green_button05"),
                        null,
                        new BitmapFont()
                )
        );
        buttonStyles.put("blue home",
                new ImageButton.ImageButtonStyle(
                        images.get("blue_button11"),
                        images.get("blue_button12"),
                        null,
                        images.get("home"),
                        null,
                        null
                )
        );
        buttonStyles.put("small green",
                new Button.ButtonStyle(
                        images.get("green_button07"),
                        images.get("green_button08"),
                        null
                )
        );
        buttonStyles.put("small green shoot",
                new ImageButton.ImageButtonStyle(
                        images.get("green_button07"),
                        images.get("green_button08"),
                        null,
                        images.get("laserRedShot"),
                        null,
                        null
                )
        );
        buttonStyles.put("small green exit",
                new ImageButton.ImageButtonStyle(
                        images.get("green_button07"),
                        images.get("green_button08"),
                        null,
                        images.get("red_cross"),
                        null,
                        null

                )
        );
    }

    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/");
        addImagesFolder("/Ui");
        addImagesFolder("/shoots");

    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
}