package ru.androiddevschool.airraid;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.airraid.Screens.Help;
import ru.androiddevschool.airraid.Screens.Menu;
import ru.androiddevschool.airraid.Screens.Play;
import ru.androiddevschool.airraid.Screens.Rating;
import ru.androiddevschool.airraid.Screens.Settings;

public class AirRaid extends Game {
	private AirRaid(){} //приватный конструктор - больше никто не может использовать кроме меня в этом классе
	private static AirRaid instance = new AirRaid(); //создание игры - вызов приватного конструктора
	public static AirRaid getInstance(){return instance;} //обеспечиваем доступ к экземпляру игры

	private SpriteBatch batch; //отрисовщик
	private HashMap<String, Screen> screens; //хранилище экранов

	@Override
	public void create() {
		//при создании игры
		batch = new SpriteBatch(); // создаю отрисовщик графики
		screens = new HashMap<String, Screen>(); //создаю хранилище экранов
		Gdx.gl.glClearColor(1,1,1,1); //цвет закрашивания экрана во всей игре будет - белый
		screens.put("Menu", new Menu(batch)); //кладу в хранилище экранов экран меню
		screens.put("Play", new Play(batch)); //кладу в хранилище экранов экран меню
		screens.put("Settings", new Settings(batch)); //кладу в хранилище экранов экран меню
		screens.put("Help", new Help(batch)); //кладу в хранилище экранов экран меню
		screens.put("Rating", new Rating(batch)); //кладу в хранилище экранов экран меню
		setScreen("Menu"); //устанавливаю экран под названием "Menu" активным
	}

	public void setScreen(String name) {
		if (screens.containsKey(name)) //если существует экран с таким названием
			setScreen(screens.get(name)); //установить его активным
	}
}
